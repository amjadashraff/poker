package au.com.me.exercise.poker.models.core;

import au.com.me.exercise.poker.models.enums.Combo;

import java.util.ArrayList;
import java.util.List;

/**
 * Holds details of a poker hand
 */
public class Hand {

    private List<Card> cards;
    private List<Combo> combos = new ArrayList<>();
    private Integer highestPairCardWeight;

    public Hand() { }

    /**
     * Constructs a new hand, with given string of cards, separated by space
     *
     * @param      hand string of cards, separated by space
     * @throws     IllegalArgumentException if the input has invalid cards
     */
    public Hand(String hand) throws IllegalArgumentException {
        List<Card> cards = new ArrayList<>();

        for (String cardString : hand.split(" "))
        {
            Card card = new Card(cardString);
            cards.add(card);
        }
        this.cards = cards;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public List<Combo> getCombos() {
        return combos;
    }

    public void setCombos(List<Combo> combos) {
        this.combos = combos;
    }

    public void addCombo(Combo combo) {
        this.combos.add(combo);
    }

    public Integer getHighestPairCardWeight() {
        return highestPairCardWeight;
    }

    public void setHighestPairCardWeight(Integer highestPairCardWeight) {
        this.highestPairCardWeight = highestPairCardWeight;
    }
}
