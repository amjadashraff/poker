package au.com.me.exercise.poker.core;

import au.com.me.exercise.poker.models.core.Card;
import au.com.me.exercise.poker.models.core.Hand;
import au.com.me.exercise.poker.models.core.Player;
import au.com.me.exercise.poker.models.enums.Combo;
import au.com.me.exercise.poker.models.enums.Faces;
import org.apache.commons.lang.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

public class GameEngine {

    /**
     *  * Given a list of hands, processes and prints player ranking
     *
     * @param       gameInputs List of strings which hold 2 hands for two players
     */
    public static void processGame(List<String> gameInputs) {

        Player player1 = new Player();
        Player player2 = new Player();
        for (String gameInput : gameInputs){
            initialisePlayers(player1, player2, gameInput);
            evaluateHands(player1, player2);
        }
        System.out.println("Player 1: " + player1.getGamesWon());
        System.out.println("Player 2: " + player2.getGamesWon());

    }

    /**
     *  * Given a list of players and hands, analyses hands and stores their card combinations
     */
    private static void initialisePlayers(Player player1, Player player2, String gameInput) throws IllegalArgumentException {
        int handSeparatorIndex = StringUtils.ordinalIndexOf(gameInput, " ", 5);
        Hand hand1 = new Hand(gameInput.substring(0, handSeparatorIndex));
        Hand hand2 = new Hand(gameInput.substring(handSeparatorIndex + 1));
        GameEngineHelper.generateRank(hand1);
        GameEngineHelper.generateRank(hand2);
        player1.setHand(hand1);
        player2.setHand(hand2);
    }

    /**
     *  * Given a list of players, analyses hands and evaluates the winner
     */
    private static void evaluateHands(Player player1, Player player2){
        List<Combo> p1Combos = player1.getHand().getCombos();
        List<Combo> p2Combos = player2.getHand().getCombos();

        List<Integer> p1Ranks = p1Combos.stream().map(Combo::getRank).collect(Collectors.toList());
        List<Integer> p2Ranks = p2Combos.stream().map(Combo::getRank).collect(Collectors.toList());

        Integer shortestRankLen = p1Ranks.size() < p2Ranks.size() ? p1Ranks.size() : p2Ranks.size();

        for (int i=0; i< shortestRankLen; i++) {
            if (p1Ranks.get(i).equals(p2Ranks.get(i))) {

                if (p1Ranks.get(i).equals(Combo.pair.getRank()) || p1Ranks.get(i).equals(Combo.two_pairs.getRank())){
                    if (player1.getHand().getHighestPairCardWeight() > player2.getHand().getHighestPairCardWeight()) {
                        player1.won();
                        break;
                    }

                    else {
                        player2.won();
                        break;
                    }
                }
                else {

                    List<Integer> p1CardWeights = player1.getHand().getCards().stream().map(Card::getFace).map(Faces::getWeight)
                            .sorted((w1,w2) -> Integer.compare(w2, w1)).collect(Collectors.toList());
                    List<Integer> p2CardWeights = player2.getHand().getCards().stream().map(Card::getFace).map(Faces::getWeight)
                            .sorted((w1,w2) -> Integer.compare(w2, w1)).collect(Collectors.toList());

                    for (int j=0; j<5; j++) {
                        if (p1CardWeights.get(j).equals(p2CardWeights.get(j))) {
                            continue;
                        }
                        else {
                            if (p1CardWeights.get(j) > p2CardWeights.get(j)) {
                                player1.won();
                                break;
                            } else {
                                player2.won();
                                break;
                            }
                        }
                    }
                }
            }
            else {
                if (p1Ranks.get(i) > p2Ranks.get(i)) {
                    player1.won();
                    break;
                }
                else {
                    player2.won();
                    break;
                }
            }
        }
    }
}
