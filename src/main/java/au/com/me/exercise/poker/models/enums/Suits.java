package au.com.me.exercise.poker.models.enums;

/**
 * This enum holds the possible Suits in a deck
 * along with the associated symbol
 */
public enum Suits {

    diamonds("D"),
    hearts("H"),
    spades("S"),
    clubs("C");

    private String symbol;

    Suits(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }

    public static String getNameBySymbol(String symbol) throws IllegalArgumentException {
        for (Suits suit: Suits.values()) {
            if (symbol.equals(suit.symbol)) return suit.name();
        }
        throw new IllegalArgumentException("Requested suit not found.");
    }
}
