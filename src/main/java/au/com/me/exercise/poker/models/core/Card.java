package au.com.me.exercise.poker.models.core;

import au.com.me.exercise.poker.models.enums.Faces;
import au.com.me.exercise.poker.models.enums.Suits;

import static au.com.me.exercise.poker.models.enums.Faces.getNameByValue;

/**
 * Holds details of a card
 */
public class Card {

    private Faces face;
    private Suits suit;

    public Card() { }

    /**
     * Constructs a new card, with given string
     *
     * @param      card string representation of cards
     * @throws     IllegalArgumentException if the input has invalid card
     */
    public Card(String card) throws IllegalArgumentException {

        String faceString = String.valueOf(card.charAt(0));
        String suitString = String.valueOf(card.charAt(1));

        this.face = Faces.valueOf(getNameByValue(faceString));
        this.suit = Suits.valueOf(Suits.getNameBySymbol(suitString));

    }

    public Faces getFace() {
        return face;
    }

    public void setFace(Faces face) {
        this.face = face;
    }

    public Suits getSuit() {
        return suit;
    }

    public void setSuit(Suits suit) {
        this.suit = suit;
    }

    public String toString() {
        return this.getFace().getValue().concat(this.getSuit().getSymbol());
    }

}
