package au.com.me.exercise.poker.models.enums;

/**
 * This enum holds the possible Face values of a card
 * along with the associated symbol and weight
 */
public enum Faces {

    two("2", 2),
    three("3", 3),
    four("4", 4),
    five("5", 5),
    six("6", 6),
    seven("7", 7),
    eight("8", 8),
    nine("9", 9),
    ten("T", 10),
    jack("J", 11),
    queen("Q", 12),
    king("K", 13),
    ace("A", 14);

    private String value;
    private Integer weight;

    Faces(String value, Integer weight) {
        this.value = value;
        this.weight = weight;
    }

    public String getValue() {
        return value;
    }

    public Integer getWeight() {
        return weight;
    }

    public static String getNameByValue(String value) throws IllegalArgumentException {
        for (Faces face: Faces.values()) {
            if (value.equals(face.value)) return face.name();
        }
        throw new IllegalArgumentException("Requested face not found.");
    }
}
