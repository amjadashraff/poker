package au.com.me.exercise.poker.models.core;

/**
 * This model is used to track a player's hand and numberof games won
 */
public class Player {

    private Hand hand;
    private Integer gamesWon = 0;

    public Hand getHand() {
        return hand;
    }

    public void setHand(Hand hand) {
        this.hand = hand;
    }

    public Integer getGamesWon() {
        return gamesWon;
    }

    public void setGamesWon(Integer gamesWon) {
        this.gamesWon = gamesWon;
    }

    /**
     * Increment the number of games won by the player
     */
    public void won(){
        this.gamesWon++;
    }

}
