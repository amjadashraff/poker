package au.com.me.exercise.poker.core;

import au.com.me.exercise.poker.models.core.Card;
import au.com.me.exercise.poker.models.core.Hand;
import au.com.me.exercise.poker.models.enums.Combo;
import au.com.me.exercise.poker.models.enums.Faces;

import java.util.*;
import java.util.stream.Collectors;

public class GameEngineHelper {

    /**
     * Returns a Hand with satisfied poker card combinations and highestPairCardWeight
     *
     * @param       hand which has a collection of cards
     * @return      hand with satisfied poker card combinations and highestPairCardWeight
     */
    public static Hand generateRank(Hand hand) {
        List<String> royals = new ArrayList<String>(){{add("T");add("J");add("Q");add("K");add("A");}};
        List<Card> cards = hand.getCards();

        if (cards.size() != 5) {
            throw new IllegalArgumentException("Wrong number of cards.");
        }

        if (new HashSet<>(cards).size() != cards.size())
        {
            throw new IllegalArgumentException("Duplicate cards found.");
        }

        int[] faceCount = new int[15];
        long straight = 0;
        for (Card card : cards) {
            int face = card.getFace().getWeight();
            straight |= (1 << face);
            faceCount[face]++;
            if (faceCount[face] == 2){
                if (hand.getHighestPairCardWeight() == null || hand.getHighestPairCardWeight() < face)
                    hand.setHighestPairCardWeight(face);
            }
        }

        // shift the bit pattern to the right as far as possible
        while (straight % 2 == 0)
            straight >>= 1;

        // straight is 00011111; A-2-3-4-5 is 1111000000001
        boolean hasStraight = straight == 0b11111 || straight == 0b1111000000001;

        boolean hasFlush = cards.stream().map(Card::getSuit).collect(Collectors.toSet()).size() == 1;

        boolean hasRoyal = false;

        if (hasStraight) {
            hasRoyal = cards.stream().map(Card::getFace).map(Faces::getValue).collect(Collectors.toSet()).containsAll(royals);
        }

        if (hasRoyal && hasFlush) {
            hand.addCombo(Combo.royal_flush);
        }

        if (hasStraight && hasFlush)
            hand.addCombo(Combo.straight_flush);

        int total = 0;
        for (int count : faceCount) {
            if (count == 4)
                hand.addCombo(Combo.four_of_a_kind);
            if (count == 3)
                total += 3;
            else if (count == 2)
                total += 2;
        }

        if (total == 5)
            hand.addCombo(Combo.full_house);

        if (hasFlush)
            hand.addCombo(Combo.flush);

        if (hasStraight)
            hand.addCombo(Combo.straight);

        if (total == 3)
            hand.addCombo(Combo.three_of_a_kind);

        if (total == 4)
            hand.addCombo(Combo.two_pairs);

        if (total == 2)
            hand.addCombo(Combo.pair);

        hand.addCombo(Combo.high_card);

        return hand;
    }
}
