package au.com.me.exercise.poker.models.enums;

/**
 * This enum holds the possible combinations of cards in a poker hand
 * along with the associated rank/weight
 */
public enum Combo {

    high_card(1),
    pair(2),
    two_pairs(3),
    three_of_a_kind(4),
    straight(5),
    flush(6),
    full_house(7),
    four_of_a_kind(8),
    straight_flush(9),
    royal_flush(10);

    private Integer rank;

    Combo(Integer rank) {
        this.rank = rank;
    }

    public Integer getRank() {
        return rank;
    }

}
