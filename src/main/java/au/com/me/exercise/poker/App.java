package au.com.me.exercise.poker;

import au.com.me.exercise.poker.core.GameEngine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main(String[] args) {

        System.out.println("Enter hands. Hit return twice to start processing.");
        List<String> hands = new ArrayList<>();
        try {
            BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
            String inputLine;
            while ((inputLine = stdin.readLine()) != null && inputLine.length()!= 0) {
                hands.add(inputLine);
            }
            System.out.println(String.format("Received [%d] games.. processing..", hands.size()));

            GameEngine.processGame(hands);

        } catch (IllegalArgumentException e) {
            System.out.println(String.format("Bad input: %s", e.getMessage()));
        } catch (IOException e) {
            System.out.println(String.format("Input could not be processed: %s", e.getMessage()));
        } catch (Exception e) {
            System.out.println(String.format("Unexpected error: %s", e.getMessage()));
        }
    }
}
