package au.com.me.exercise.poker.test;


import au.com.me.exercise.poker.core.GameEngineHelper;
import au.com.me.exercise.poker.models.core.Card;
import au.com.me.exercise.poker.models.core.Hand;
import au.com.me.exercise.poker.models.enums.Combo;
import au.com.me.exercise.poker.models.enums.Faces;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

public class HelperTests {

    @Test
    public void testHandRankerForHighCard() {
        Hand hand = new Hand("2H 3D 5S 8D AC");
        GameEngineHelper.generateRank(hand);
        List<Integer> weights = hand.getCards().stream().map(Card::getFace).map(Faces::getWeight)
                .sorted((w1, w2) -> Integer.compare(w2, w1)).collect(Collectors.toList());
        assert hand.getCombos().size() == 1;
        assert hand.getCombos().contains(Combo.high_card);
        assert weights.get(0) == 14;
    }

    @Test
    public void testHandRankerForPair() {
        Hand hand = new Hand("2H 3D 2S 8D AC");
        GameEngineHelper.generateRank(hand);
        assert hand.getCombos().contains(Combo.pair);
    }

    @Test
    public void testHandRankerForTwoPair() {
        Hand hand = new Hand("2H AD 2S 8D AC");
        GameEngineHelper.generateRank(hand);
        assert hand.getCombos().contains(Combo.two_pairs);
        assert hand.getHighestPairCardWeight() == 14;
    }

    @Test
    public void testHandRankerForThreeOfAKind() {
        Hand hand = new Hand("2H 2D 2S 8D AC");
        GameEngineHelper.generateRank(hand);
        assert hand.getCombos().contains(Combo.three_of_a_kind);
    }

    @Test
    public void testHandRankerForStraight() {
        Hand hand = new Hand("2H 3D 4S 5D 6C");
        GameEngineHelper.generateRank(hand);
        assert hand.getCombos().contains(Combo.straight);
    }

    @Test
    public void testHandRankerForFlush() {
        Hand hand = new Hand("2H 3H 2H 8H AH");
        GameEngineHelper.generateRank(hand);
        assert hand.getCombos().contains(Combo.flush);
    }

    @Test
    public void testHandRankerForFullHouse() {
        Hand hand = new Hand("2H 2D 2S 8D 8C");
        GameEngineHelper.generateRank(hand);
        assert hand.getCombos().contains(Combo.full_house);
    }

    @Test
    public void testHandRankerForFourOfAKind() {
        Hand hand = new Hand("2H 2D 2S 2D AC");
        GameEngineHelper.generateRank(hand);
        assert hand.getCombos().contains(Combo.four_of_a_kind);
    }

    @Test
    public void testHandRankerForStraightFlush() {
        Hand hand = new Hand("2H 3H 4H 5H 6H");
        GameEngineHelper.generateRank(hand);
        assert hand.getCombos().contains(Combo.straight_flush);
    }

    @Test
    public void testHandRankerForRoyalFlush() {
        Hand hand = new Hand("JH QH KH AH TH");
        GameEngineHelper.generateRank(hand);
        assert hand.getCombos().contains(Combo.royal_flush);
    }

}
