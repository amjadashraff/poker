package au.com.me.exercise.poker.test;

import au.com.me.exercise.poker.models.core.Card;
import au.com.me.exercise.poker.models.core.Hand;
import au.com.me.exercise.poker.models.enums.Faces;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

public class ModelTests {

    @Test
    public void testModelInit(){
        Hand hand = new Hand("2H 3D 2S 8D AC");
        List<Integer> weights = hand.getCards().stream().map(Card::getFace).map(Faces::getWeight)
                .sorted((w1,w2) -> Integer.compare(w2, w1)).collect(Collectors.toList());
        assert hand.getCards().size() == 5;
        assert weights.get(0).equals(14);
        assert weights.get(4).equals(2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFaceInitErr() {
        new Hand("0H 3D 2S 8D AC");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSuitInitErr() {
        new Hand("2B 3D 2S 8D AC");
    }
}
